# app-extension-apollo

## Introduction

This is the official Quasar app extension for adding GraphQL to your Quasar
project.

It uses [Apollo Client](https://www.apollographql.com) and [Vue Apollo](https://apollo.vuejs.org).

## Installation

```sh
quasar ext add @quasar/apollo
```

Quasar CLI will retrieve the extension from NPM
([@quasar/quasar-app-extension-apollo](https://www.npmjs.com/package/@quasar/quasar-app-extension-apollo))

The extension will add the boot file: `src/boot/apollo.ts`

### Prompts

You will be prompted to enter the URI of your GraphQL endpoint. You can skip
this and instead provide the URI using an environment variable when running
Quasar:

```sh
GRAPHQL_URI=https://prod.example.com/graphql quasar build
GRAPHQL_URI=https://dev.example.com/graphql quasar dev
```

If you don't have a GraphQL endpoint yet, you can create one to experiment
with at [FakeQL](https://fakeql.com) or other similar services.

### App.vue

Modify `src/App.vue` as shown below:

```ts
 ...
import { provide } from 'vue'
import { DefaultApolloClient } from '@vue/apollo-composable'
import { apolloClient } from 'src/boot/apollo'

 export default defineComponent({
 ...
  setup () {
    provide(DefaultApolloClient, apolloClient)
  }
 })
```

## Uninstall

```sh
quasar ext remove @quasar/apollo
```

**Warning** Added files will be removed, if you need, make a backup before
uninstalling the extension.

## Apollo client options

Apollo client options can be customized in `src/apollo/options.ts`.

## Usage

Check the guide in [Vue Apollo docs](https://v4.apollo.vuejs.org/guide-composable/setup.html).

Example usage:

`src/pages/Index.vue`

```html
<template>
  <q-page class="row items-center justify-evenly">

    <div v-if="loading">Loading...</div>
    <div v-else-if="error">Error: {{ error.message }}</div>
    <div v-else-if="result && result.post">
      <div>id: {{ result.post.id }}</div>
      <div>title: {{ result.post.title }}</div>
    </div>

    ...
  </q-page>
</template>

<script lang="ts">
...
import { useQuery } from '@vue/apollo-composable'
import gql from 'graphql-tag'

export default defineComponent({
  ...
  setup () {
     ...
    const { result, loading, error } = useQuery(gql`
      query getPosts {
        post(id: "3") {
          id
          title
        }
      }
    `)

    return { /* your other items, */ result, loading, error }
  }
})
</script>
```

## Multiple apollo clients setup

Modify the following files as shown:

`src/boot/apollo.ts`
```ts
import { boot } from 'quasar/wrappers'
import { ApolloClient, createHttpLink } from '@apollo/client/core'
import { getClientOptions } from 'src/apollo/options'
import type { BootFileParams } from '@quasar/app'

let apolloClients: Record<string, ApolloClient<unknown>>

// bootFileParams is { app, router, ...}
export default boot(async (bootFileParams: BootFileParams<unknown>) => {
  // Default client.
  const options = await getClientOptions(bootFileParams)
  const apolloClient = new ApolloClient(options)

  // Additional client `clientA`
  const optionsA = { ...options }
  // Modify options as needed.
  optionsA.link = createHttpLink({ uri: 'http://clientA.example.com' })
  const clientA = new ApolloClient(optionsA)

  // Additional client `clientB`
  const optionsB = { ...options }
  // Modify options as needed.
  optionsB.link = createHttpLink({ uri: 'http://clientB.example.com' })
  const clientB = new ApolloClient(optionsB)

  apolloClients = {
    default: apolloClient,
    clientA,
    clientB
  }
})

export { apolloClients }
```

`src/App.vue`
```html
<template>
  <router-view />
</template>
<script lang="ts">
import { defineComponent, provide } from 'vue'
import { ApolloClients } from '@vue/apollo-composable'
import { apolloClients } from 'src/boot/apollo'

export default defineComponent({
  name: 'App',
  setup () {
    provide(ApolloClients, apolloClients)
  }
})
</script>
```

To use `clientA` instead of the default client:

`src/pages/Index.vue`
```ts
    ...
    const { result, loading, error } = useQuery(gql`
      query getPosts {
        post(id: "3") {
          id
          title
        }
      }
    `, null, { clientId: 'clientA' })
    ...
```
