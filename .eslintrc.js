module.exports = {
  root: true,

  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true
  },

  extends: [
    'plugin:vue/vue3-recommended',
    'standard'
  ],

  parserOptions: {
    ecmaVersion: 12,
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },

  plugins: [
    'vue',
    '@typescript-eslint'
  ],

  rules: {
  }
}
