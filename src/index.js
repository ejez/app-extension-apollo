module.exports = function (api) {
  api.extendQuasarConf((conf) => {
    // Register the boot file.
    conf.boot.push('apollo')

    // Make sure the app extension files get transpiled.
    // https://quasar.dev/app-extensions/development-guide/index-api#Registering-boot-and-css-files
    conf.build.transpileDependencies.push(/quasar-app-extension-apollo[\\/]src/)

    // Allow overriding of graphql uri using an env variable
    // https://quasar.dev/quasar-cli/handling-process-env#Adding-to-process.env
    conf.build.env.GRAPHQL_URI = process.env.GRAPHQL_URI
  })
}
